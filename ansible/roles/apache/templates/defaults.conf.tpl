<VirtualHost *:80>
    DocumentRoot "/var/www/rockmongo"
    ServerName dev-rockmongo.com
    ServerAlias www.dev-rockmongo.com
    <Directory "/var/www/rockmongo">
        AllowOverride None
        Options None
        Order allow,deny
        Allow from all
        Options +FollowSymLinks +SymLinksIfOwnerMatch
    </Directory>
</VirtualHost>